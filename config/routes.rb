Rails.application.routes.draw do





  get '/', to: redirect('/ru')

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  #devise_for :users, skip: [:session, :password, :registration, :confirmation], controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  scope ':locale' do

    resources :categories
    resources :instructions do
      member do
        put "like" => "instructions#upvote"
        put "dislike" => "instructions#downvote"
        put "unvote" => "instructions#unvote"
        get 'instructions/destroyimage/(:image_id)', to: 'instructions#destroyimage', as: 'destroyimage'
      end
    end

    resources :steps do
      resources :comments
    end

    resources :users
    post 'banned/:id', to: "users#banned", :as => :banned_user
    post 'manager/:id', to: "users#manager", :as => :manager_user


    devise_scope :user do
      get 'sign_in', :to => 'devise/sessions#new', :as => :new_user_session
      get 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
      get 'omniauth/:provider' => 'omniauth#localized', as: :localized_omniauth
      #get '/users/auth/:provider', to: "users/omniauth_callbacks#authorisate", as: :user_omniauth_authorize
      devise_for :users, skip: [:omniauth_callbacks]


    end

    get 'show', :to => 'home#show'

    root 'home#index'

  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
