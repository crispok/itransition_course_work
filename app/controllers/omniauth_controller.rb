class OmniauthController < ApplicationController
  def localized
    # Just save the current locale in the session and redirect to the unscoped path as before
    session[:omniauth_login_locale] = I18n.locale
    request.env['omniauth.origin'] = params[:origin]
    redirect_to user_omniauth_authorize_path(params[:provider])
  end

  def user_omniauth_authorize_path(resource_name)
    send "user_#{resource_name}_omniauth_authorize_path"
  end


end