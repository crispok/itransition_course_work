class HomeController < ApplicationController
  def index
    @instructions = Instruction.eager_load(:category, :user ).limit(10).order(cached_votes_score: :asc)
  end

  def show
  end
end
