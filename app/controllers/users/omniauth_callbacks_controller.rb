class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  include Devise::Controllers::Rememberable

  def authorisate
    @user = User.from_omniauth(request.env["omniauth.auth"])
    redirect_url = CGI.parse(URI.parse(request.env['omniauth.origin']).query)['origin'].first
    I18n.locale = redirect_url.split("/").second
    if @user.persisted?
      remember_me(@user)
      sign_in @user
      redirect_to redirect_url
      set_flash_message(:success, :success, :kind => request.env["omniauth.auth"].provider.capitalize) if is_navigational_format?
    else
      puts @user.errors.messages
      session["devise.data"] = request.env["omniauth.auth"].except('extra')
      set_flash_message(:danger, :failure, :kind => request.env["omniauth.auth"].provider.capitalize, :reason =>  t(@user.errors.messages.each.first.join(": "))) if is_navigational_format?
      redirect_to redirect_url
    end
  end

  def failure
    redirect_to root_path
  end
  def twitter
    authorisate
  end

  alias_method :facebook, :authorisate
  alias_method :twitter, :authorisate
  alias_method :vkontakte, :authorisate


end
