class InstructionsController < ApplicationController
  before_action :set_instruction, only: [:show, :edit, :update, :destroy, :upvote, :downvote, :unvote]
  # GET /instructions
  # GET /instructions.json
  def index
    @instructions = Instruction.eager_load(:category).all.order(created_at: :desc)
  end

  # GET /instructions/1
  # GET /instructions/1.json
  def show
    @like = find_like
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "file_name",
               encoding: "utf-8",
               layout: "application.pdf.erb",
               disable_internal_links: false,
               disable_external_links: false,
               disable_links:          false,
               disable_toc_links:      false,
               disable_back_links:     false
      end
    end
  end

  def destroyimage
    Cloudinary::Uploader.destroy("#{params[:id]}")
    @index = params[:index]
  end

  # GET /instructions/new
  def new
    @instruction = Instruction.new
    authorize! :create, @instruction
    @categories = Category.all.map{|c| [ c.name, c.id ] }
  end

  # GET /instructions/1/edit
  def edit
    authorize! :update, @instruction
    @categories = Category.all.map{|c| [ c.name, c.id ] }
    @index = 0
  end

  # POST /instructions
  # POST /instructions.json
  def create
    @instruction = Instruction.new(instruction_params)
    @categories = Category.all.map{|c| [ c.name, c.id ] }
    @instruction.category_id = params[:category_id]
    @instruction.user_id = current_user['id']
    respond_to do |format|
      if @instruction.save
        format.html { redirect_to @instruction, notice: 'Instruction was successfully created.' }
        format.json { render :show, status: :created, location: @instruction }
      else
        format.html { render :new }
        format.json { render json: @instruction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instructions/1
  # PATCH/PUT /instructions/1.json
  def update

    @categories = Category.all.map{|c| [ c.name, c.id ] }
    respond_to do |format|

      if @instruction.update(instruction_params)
        format.html { redirect_to @instruction, notice: 'Instruction was successfully updated.' }
        format.json { render :show, status: :ok, location: @instruction }
      else
        format.html { render :edit }
        format.json { render json: @instruction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /instructions/1
  # DELETE /instructions/1.json
  def destroy
    authorize! :destroy, @instruction
    @instruction.destroy
    respond_to do |format|
      format.html { redirect_to instructions_url, notice: 'Instruction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def upvote
    @instruction.vote_up current_user
  end

  def downvote
    @instruction.vote_down current_user
  end

  def unvote
    @instruction.unvote_by current_user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instruction
      @instruction = Instruction.eager_load(:category, steps: :comments).where(id: params[:id]).order('steps.index').to_a.first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def instruction_params
      params.require(:instruction).permit(:title, :body, :category_id, :user_id, :youtube_url, steps_attributes: [:title, :body, :image, :_destroy, :id, :index])
    end

    def find_like

      if user_signed_in?
        buf = Votes.find_by(voter_id: current_user.id, votable_id: @instruction.id)
        buf == nil ? nil : buf.vote_flag
      else
        return nil
      end

    end

end
