class CommentsController < ApplicationController

  before_action :authenticate_user!
  before_action :find_comment, only: :destroy
  load_and_authorize_resource through: :current_user
  load_and_authorize_resource param_method: :comment_params

  def create
    @comment = Comment.new(comment_params)
    unless @comment['body'].empty?
      puts "wefwgwefwe"
      @comment.user_id = current_user['id']
      @comment.step_id = params['step_id']
      @comment.save

    end
  end

  def destroy
    @comment.destroy if current_user.id == @comment.user_id
  end


  private

  def find_comment
    @comment = Comment.find(params[:comment_id])
  end

  def comment_params
    params.require(:comment).permit(:body, :step_id)
  end

end
