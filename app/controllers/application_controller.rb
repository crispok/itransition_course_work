class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale


  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    Time.zone = "Minsk"
  end

  def default_url_options
    { locale: I18n.locale }
  end

  private
  def new_session_path(scope)
    new_user_session_path
  end

  def after_sign_out_path_for(resource_or_scope)
    request.referrer || root_path
  end

end
