class Step < ApplicationRecord
  belongs_to :instruction, optional: true
  has_many :comments

  acts_as_voter

  validates :title, presence: true
  validates :body, presence: true
  validates :image, presence: true

end
