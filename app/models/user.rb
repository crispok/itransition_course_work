class User < ApplicationRecord
  devise :rememberable, :trackable, :omniauthable, :omniauth_providers => [:facebook, :vkontakte, :twitter]

  has_many :comments
  has_many :instructions
  validates :email, presence: true

  def self.from_omniauth(auth)
      user = User.find_or_initialize_by(email: auth.info.email)
      if user.banned
        user = User.new
        user.errors.add(:failure, 'banned')
        return user
      end
      user.name, user.image  = auth.info.name, auth.info.image
      user.save if user.changed?
      return user
  end

end
