class Instruction < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_many :steps

  acts_as_votable

  accepts_nested_attributes_for :steps, allow_destroy: true
  validates :title, presence: true
  validates :body, presence: true
  validates :steps, :length => { :minimum => 1 }
  validates_associated :steps
end
