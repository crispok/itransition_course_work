module StepsHelper

  def deserialize_image(url)
      url.split(%r{image\/upload\/v[0-9]*\/(.*?)\.}).second
  end

end
