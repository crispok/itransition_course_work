module InstructionsHelper
  def youtube_embded(youtube_url)
    youtube_id = youtube_url.split("=").last
    content_tag(:iframe, nil, src: "//www.youtube.com/embed/#{youtube_id}", width: '100%', height: '350px',  frameborder: 0)
  end

  def qr(url)
    RQRCode::QRCode.new( url, :size => 5, :level => :h )
  end

  def index(index)
    index.split("instruction[steps_attributes][").second[0...-1]
  end

  def image_publick_id(form)
    form =~  /value="(\w*)"/ ? form.scan( /value="(\w*)"/).last.first.to_s : ""
  end

  def display(image)
    "display:" + (image.to_s.empty? ? "block" : "none")
  end
end
