module ApplicationHelper

  def li_link_to(name = nil, options = nil, html_options = nil, &block)
    html_options, options, name = options, name, block if block_given?
    options ||= {}
    html_options = convert_options_to_data_attributes(options, html_options)
    url = url_for(options)
    html_options["href".freeze] ||= url
    html_options['li-class'] +=  current_page?(options) ? " active" : ""
    content_tag(:li, content_tag("a".freeze, name || url, html_options.except("li-class"), &block), class: html_options['li-class'])
  end

  def show_svg(path)
    File.open("vendor/assets/#{path}", "rb") do |file|
      raw file.read
    end
  end



end

