var position = 0;
$(document).on('turbolinks:load', function() {
    $( "#sortable" ).sortable(
        {
            update: function( event, ui ) {
                update_steps();
            }
        }
    );
    $( "#sortable" ).disableSelection();
    $(function() {
        if($.fn.cloudinary_fileupload !== undefined) {
            $("input.cloudinary-fileupload[type=file]").cloudinary_fileupload();
        }
    });
    $(".cloudinary-fileupload").each(cloudinary_initializer);

    $( 'textarea').each( function(index) {
        if ("step_comment" != $(this).attr('id'))
            CKEDITOR.replace($(this).attr('id'));

    });
    var owl = $('.owl-carousel');



    $('#myModal').on('shown.bs.modal', function (e) {

        owl.owlCarousel({
            items: 1,
            margin: 10,
            autoHeight: true,
            startPosition: position

        });


        resize_modal();
    });
    $('#myModal').on('hidden.bs.modal', function (e) {
        owl.trigger('destroy.owl.carousel');
    });
    owl.on('translated.owl.carousel', function(event) {
        resize_modal();

    });
    update_steps();
} );

function cloudinary_initializer() {

        var field = $(this);
        var line = field.parents('.form_line');
        field.fileupload({
            dropZone: line,
            start: function (e) {
                $(line).find(".progress-bar").text(I18n.t("start_upload"));
                $(line).find("#direct_upload").css('display','none');
            },
            progress: function (e, data) {
                $(line).find(".progress-bar").text( I18n.t("status_upload") + ": " + Math.round((data.loaded * 100.0) / data.total) + "%");
                $(line).find(".progress-bar").css("width", Math.round((data.loaded * 100.0) / data.total) + "%");
            },
            fail: function (e, data) {
                $(line).find(".progress-bar").text(I18n.t("failed_upload"));
            }
        })
            .off("cloudinarydone").on("cloudinarydone", function (e, data) {

            $(line).find(".progress-bar").text(I18n.t("done_upload"));
            var preview = $(line).find(".preview").html('');
            $.cloudinary.image(data.result.public_id, {
                format: data.result.format, quality: "auto:low",  crop: "fit", class: "img-thumbnail"
            }).appendTo(preview);
            $('<a/>').
            addClass('delete_by_token').
            attr({href: '#'}).
            data({delete_token: data.result.delete_token}).
            html('<i class="fa fa-trash" aria-hidden="true"></i>').
            appendTo(preview).
            click(function(e) {
                e.preventDefault();
                $.cloudinary.delete_by_token($(this).data('delete_token')).done(function(){
                    $('.preview').html('');
                    $('#info').html('');
                    $("#photo_bytes").val('');
                    $('input[name="photo[image]"]').remove();
                    $(line).find("#direct_upload").css('display','block');
                }).fail(function() {
                    $('.progress-bar').text("Cannot delete image");
                });
            });
            console.log($(this));
            view_upload_details(data.result);
            console.log(data.result.public_id);
            $(line).find(".hidden_field").val(data.result.public_id)
        });


}

function view_upload_details(upload) {
    // Build an html table out of the upload object
    var rows = [];
    $.each(upload, function(k,v){
        rows.push(
            $("<tr>")
                .append($("<td>").text(k))
                .append($("<td>").text(JSON.stringify(v))));
    });
    $("#info").html(
        $("<div class=\"upload_details\">")
            .append("<h2>Upload metadata:</h2>")
            .append($("<table>").append(rows)));
    console.log(rows);
}

function update_steps() {
    $("number").each(function( index ){
        $(this).html(index+1)
    });
    $("input").filter('[data-order="true"]').each(function (index) {
        $(this).val(index);
    })

}

$(document).on("fields_added.nested_form_fields",function(event, param){
    $(".cloudinary-fileupload").last().cloudinary_fileupload();
    $(".cloudinary-fileupload").each(cloudinary_initializer);
    CKEDITOR.replace($('textarea').last().attr('id'));
    update_steps();
});
$(document).on("fields_removing.nested_form_fields",function(event, param){
    $("#instruction_steps_attributes_"+param.removed_index+"_index").parent().find('number').remove();
    update_steps();
});

function goStep(index) {
    position = index
    $('#myModal').modal('show');
    $('.owl-carousel').trigger('to.owl.carousel',position);

}

function resize_modal() {
    $('.modal-body').css("height", $(".owl-item.active").height()+20);
};


