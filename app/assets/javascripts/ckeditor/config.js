if (typeof(CKEDITOR) != 'undefined') {
    CKEDITOR.editorConfig = function( config )
    {
        config.language = I18n.locale;
        config.toolbar = 'MyToolbar'
        config.toolbar_MyToolbar =
            [
                { name: 'basicstyles', items : ["Bold",  "Italic",  "Underline",  "Strike",  "-",  "Subscript",  "Superscript"] },

                { name: 'paragraph', items : [ 'BulletedList', 'NumberedList', 'Blockquote' ] },

            ];
        config.removePlugins = 'elementspath';




    }
}