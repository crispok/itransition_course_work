class CreateInstructions < ActiveRecord::Migration[5.0]
  def change
    create_table :instructions do |t|
      t.string :title, null: false
      t.text :body, null: false
      t.string :category_id, null: false
      t.string :user_id, null: false

      t.timestamps
    end
  end
end
