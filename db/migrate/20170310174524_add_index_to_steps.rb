class AddIndexToSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :steps, :index, :integer
  end
end
