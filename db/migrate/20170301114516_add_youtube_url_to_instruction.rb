class AddYoutubeUrlToInstruction < ActiveRecord::Migration[5.0]
  def change
    add_column :instructions, :youtube_url, :string
  end
end
