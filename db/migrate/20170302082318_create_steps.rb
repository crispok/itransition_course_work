class CreateSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :steps do |t|
      t.string :title, null: false
      t.text :body, null: false
      t.string :image, null: false
      t.string :instruction_id

      t.timestamps
    end
    add_index :steps, :instruction_id
  end
end
